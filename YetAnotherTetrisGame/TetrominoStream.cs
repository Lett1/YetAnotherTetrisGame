﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="TetrominoStream.cs" company="LettSoft">
//   LettSoft 2018
// </copyright>
// <summary>
//   Provides a random stream of tetrominos.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace YetAnotherTetrisGame
{
    using System;

    using YetAnotherTetrisGame.Enums;

    /// <summary>
    /// Provides a random stream of tetrominos.
    /// </summary>
    public class TetrominoStream
    {
        /// <summary>
        /// The size of the stream.
        /// </summary>
        private const int Streamsize = 3;

        /// <summary>
        /// The array that holds stream data.
        /// </summary>
        private TetrominoType[] stream;

        /// <summary>
        /// Initializes a new instance of the <see cref="TetrominoStream"/> class.
        /// </summary>
        public TetrominoStream()
        {
            this.stream = new TetrominoType[Streamsize];

            for (int i = 0; i < this.stream.GetLength(0); i++)
            {
                this.stream[i] = (TetrominoType)Utilities.Random.Next(1, Enum.GetNames(typeof(TetrominoType)).Length);
            }
        }

        /// <summary>
        /// Gets the next value in the stream.
        /// </summary>
        /// <returns>
        /// The <see cref="TetrominoType"/>.
        /// </returns>
        public TetrominoType Peek()
        {
            return this.stream[0];
        }

        /// <summary>
        /// Pops the next value of the stream and adds a new one.
        /// </summary>
        /// <returns>
        /// The next Tetromino to spawn.
        /// </returns>
        public TetrominoType Pop()
        {
            TetrominoType nextType = this.stream[0];

            for (int i = 0; i < this.stream.GetLength(0) - 1; i++)
            {
                this.stream[i] = this.stream[i + 1];
            }

            this.stream[this.stream.GetLength(0) - 1] = (TetrominoType)Utilities.Random.Next(
                1,
                Enum.GetNames(typeof(TetrominoType)).Length);

            return nextType;
        }
    }
}