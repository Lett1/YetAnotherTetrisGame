﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IntervalTimerThreadArguments.cs" company="Lettsoft">
//   LettSoft 2018
// </copyright>
// <summary>
//   Arguments for the IntervalTimer thread.
//   Base class structure taken from WS2017_BIT_1_Grundlagen der Programmierung_28.11.2017.pdf, page 33.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace YetAnotherTetrisGame
{
    /// <summary>
    /// Interval timer thread arguments.
    /// </summary>
    public class IntervalTimerThreadArguments
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="T:YetAnotherTetrisGame.IntervalTimerThreadArguments"/> class.
        /// </summary>
        /// <param name="interval">
        /// Time between ticks in milliseconds.
        /// </param>
        public IntervalTimerThreadArguments(int interval)
        {
            this.Exit = false;
            this.Interval = interval;
        }

        /// <summary>
        /// Gets or sets a value indicating whether the timer should exit or not.
        /// </summary>
        /// <value>
        /// Whether the timer should exit or not.
        /// </value>
        public bool Exit { get; set; }

        /// <summary>
        /// Gets or sets the interval.
        /// </summary>
        /// <value>
        /// The interval between ticks in milliseconds.
        /// </value>
        public int Interval { get; set; }
    }
}