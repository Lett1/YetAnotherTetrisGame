﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IntervalTimer.cs" company="LettSoft">
//   LettSoft 2018
// </copyright>
// <summary>
//   A timer that fires an event in regular intervals.
//   Base class structure taken from WS2017_BIT_1_Grundlagen der Programmierung_28.11.2017.pdf, page 28 to 32.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace YetAnotherTetrisGame
{
    using System;
    using System.Threading;

    /// <summary>
    /// A timer that fires an event in regular intervals.
    /// </summary>
    public class IntervalTimer
    {
        /// <summary>
        /// The thread that periodically sends interval events.
        /// </summary>
        private Thread thread;

        /// <summary>
        /// Arguments for the thread that sends interval events.
        /// </summary>
        private IntervalTimerThreadArguments threadArguments;

        /// <summary>
        /// Initializes a new instance of the <see cref="IntervalTimer"/> class.
        /// </summary>
        /// <param name="interval">
        /// The interval.
        /// </param>
        public IntervalTimer(int interval)
        {
            this.threadArguments = new IntervalTimerThreadArguments(interval);
        }

        /// <summary>
        /// Is fired when the timer has started.
        /// </summary>
        public event EventHandler AfterStarted;

        /// <summary>
        /// Is fired when the timer has stopped.
        /// </summary>
        public event EventHandler AfterStopped;

        /// <summary>
        /// Is fired when an intervall has passed.
        /// </summary>
        public event EventHandler OnIntervalPassed;

        /// <summary>
        /// Gets the interval between events.
        /// </summary>
        /// <value>The interval in ms.</value>
        public int Interval
        {
            get { return this.threadArguments.Interval; }
        }

        /// <summary>
        /// Change the interval between events.
        /// </summary>
        /// <param name="newInterval">
        /// The new interval.
        /// </param>
        /// <exception cref="ArgumentOutOfRangeException">
        /// Thrown if the supplied interval is less than 0 milliseconds.
        /// </exception>
        public void ChangeInterval(int newInterval)
        {
            if (newInterval < 0)
            {
                throw new ArgumentOutOfRangeException(
                    nameof(newInterval),
                    "Interval cannot be less than 0 milliseconds.");
            }

            this.threadArguments.Interval = newInterval;
        }

        /// <summary>
        /// Starts the timer.
        /// </summary>
        /// <exception cref="InvalidOperationException">
        /// Is thrown if the timer is already started.
        /// </exception>
        public void Start()
        {
            if (this.thread != null && this.thread.IsAlive)
            {
                throw new InvalidOperationException("The IntervalTimer is already running.");
            }

            this.threadArguments.Exit = false;
            this.thread = new Thread(this.Worker);
            this.thread.Start(this.threadArguments);
        }

        /// <summary>
        /// Stops the timer.
        /// </summary>
        /// <exception cref="InvalidOperationException">
        /// Is thrown if the timer is already stopped.
        /// </exception>
        public void Stop()
        {
            if (this.thread == null || !this.thread.IsAlive)
            {
                throw new InvalidOperationException("The IntervalTimer is already stopped.");
            }

            this.threadArguments.Exit = true;
            this.thread.Join();
        }

        /// <summary>
        /// Fires the <see cref="AfterStarted"/> event.
        /// </summary>
        protected virtual void FireAfterStarted()
        {
            if (this.AfterStarted != null)
            {
                this.AfterStarted(this, EventArgs.Empty);
            }
        }

        /// <summary>
        /// Fires the <see cref="AfterStopped"/> event.
        /// </summary>
        protected virtual void FireAfterStopped()
        {
            if (this.AfterStopped != null)
            {
                this.AfterStopped(this, EventArgs.Empty);
            }
        }

        /// <summary>
        /// Fires the <see cref="OnIntervalPassed"/> event.
        /// </summary>
        protected virtual void FireOnIntervalPassed()
        {
            if (this.OnIntervalPassed != null)
            {
                this.OnIntervalPassed(this, EventArgs.Empty);
            }
        }

        /// <summary>
        /// Represents the worker thread that sends OnInterval events.
        /// </summary>
        /// <param name="data">
        /// The thread arguments.
        /// </param>
        /// <exception cref="ArgumentOutOfRangeException">
        /// Is thrown if the specified instance for data is not of type <see cref="IntervalTimerThreadArguments"/>.
        /// </exception>
        private void Worker(object data)
        {
            if (!(data is IntervalTimerThreadArguments))
            {
                throw new ArgumentOutOfRangeException(
                    nameof(data),
                    $"The specified data must be an instance of the {nameof(IntervalTimerThreadArguments)} class.");
            }

            IntervalTimerThreadArguments args = (IntervalTimerThreadArguments)data;
            this.FireAfterStarted();
            while (!args.Exit)
            {
                Thread.Sleep(args.Interval);
                this.FireOnIntervalPassed();
            }

            this.FireAfterStopped();
        }
    }
}