﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Vector2.cs" company="LettSoft">
//   LettSoft 2018
// </copyright>
// <summary>
//   Defines the Vector2 struct.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace YetAnotherTetrisGame
{
    /// <summary>
    /// Represents a pair of coordinates in 2d space.
    /// </summary>
    public struct Vector2
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Vector2"/> struct. 
        /// </summary>
        /// <param name="x">
        /// The X coordinate.
        /// </param>
        /// <param name="y">
        /// The Y coordinate.
        /// </param>
        public Vector2(int x, int y)
        {
            this.X = x;
            this.Y = y;
        }

        /// <summary>
        /// Gets or sets the X Coordinate of the Vector.
        /// </summary>
        /// <value>
        /// The X Coordinate of the Vector.
        /// </value>
        public int X { get; set; }

        /// <summary>
        /// Gets or sets the Y Coordinate of the Vector.
        /// </summary>
        /// <value>
        /// The Y Coordinate of the Vector.
        /// </value>
        public int Y { get; set; }

        /// <summary>
        /// Add a Vector to this instance.
        /// </summary>
        /// <param name="otherVector">
        /// The Vector to add to this instance.
        /// </param>
        /// <returns>
        /// The sum of both Vectors.
        /// </returns>
        public Vector2 Add(Vector2 otherVector)
        {
            return new Vector2(this.X + otherVector.X, this.Y + otherVector.Y);
        }

        /// <summary>
        /// Checks whether two vectors are equal, both components are equal.
        /// </summary>
        /// <param name="otherVector">
        /// The other Vector.
        /// </param>
        /// <returns>
        /// True if both Vectors are equal.
        /// </returns>
        public bool Equals(Vector2 otherVector)
        {
            return this.X == otherVector.X && this.Y == otherVector.Y;
        }

        /// <summary>
        /// Returns a string representation of the Vector in the format "X:{x coordinate}, Y:{y coordinate}".
        /// </summary>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        public override string ToString()
        {
            return $"X:{this.X}, Y:{this.Y}";
        }
    }
}