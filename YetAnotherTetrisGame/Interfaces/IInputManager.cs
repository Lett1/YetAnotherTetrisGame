﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IInputManager.cs" company="LettSoft">
//   LettSoft 2018
// </copyright>
// <summary>
//   Defines the IInputManager type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace YetAnotherTetrisGame.Interfaces
{
    using System;

    /// <summary>
    /// The InputManager interface.
    /// </summary>
    public interface IInputManager
    {
        /// <summary>
        /// Whether an has been received but not processed yet.
        /// </summary>
        /// <returns>
        /// True if inputs have been received but not processed yet.
        /// </returns>
        bool InputAvailable();

        /// <summary>
        /// Read a single Key from input.
        /// </summary>
        /// <returns>
        /// The <see cref="ConsoleKey"/>.
        /// </returns>
        ConsoleKey ReadKey();

        /// <summary>
        /// Read a string from standard input.
        /// </summary>
        /// <returns>
        /// The entered string.
        /// </returns>
        string ReadLine();

        /// <summary>
        /// Wait for a key press of one or more specific keys. Or just any key.
        /// </summary>
        /// <param name="keys">
        /// The keys to wait for.
        /// </param>
        void WaitForKeyPress(params ConsoleKey[] keys);
    }
}