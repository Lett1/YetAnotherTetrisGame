﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Program.cs" company="LettSoft">
//   LettSoft 2018
// </copyright>
// <summary>
//   The main program class.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace YetAnotherTetrisGame
{
    using System;

    /// <summary>
    /// Main program class.
    /// </summary>
    internal static class Program
    {
        /// <summary>
        /// The entry point of the program, where the program control starts and ends.
        /// </summary>
        /// <param name="args">
        /// The command-line arguments.
        /// </param>
        public static void Main(string[] args)
        {
            int level = 1;
            
            if (args.Length > 0)
            {
                string[] argument = args[0].Split(':');
                if (argument.Length <= 1 || !argument[0].StartsWith("/") || !IsNumber(argument[1]))
                {
                    Console.WriteLine("Error: Invalid Arguments");
                    Console.WriteLine("Valid Arguments: /Level:N where N is a number greater than 0. Sets the starting level.");
                    Environment.Exit(1);
                }

                try
                {
                    level = int.Parse(argument[1]);
                }
                catch (Exception e)
                {
                    Console.WriteLine("Error trying to parse arguments:");
                    Console.WriteLine(e.Message);
                    throw;
                }

                if (level <= 0)
                {
                    Console.WriteLine("Error: Level must be greater than 0");
                    Environment.Exit(1);
                }
            }

            while (true)
            {
                TetrisGame game = new TetrisGame(level);

                // Run the game and check if the user exited manually
                if (!game.Run())
                {
                    PrintGameOver();

                    Console.ReadLine();

                    if (!Utilities.AskForBoolean("Do you want to play again?"))
                    {
                        Console.WriteLine("Goodbye!");
                        return;
                    }
                }
                else
                {
                    // This path is hit if the user exited the game with the Esc key.
                    return;
                }
            }
        }

        /// <summary>
        /// Checks if a string only consists of numbers.
        /// </summary>
        /// <returns><c>true</c>, if only numbers are in the string, <c>false</c> otherwise.</returns>
        /// <param name="text">The string to check.</param>
        private static bool IsNumber(string text)
        {
            for (int i = 0; i < text.Length; i++)
            {
                if (!char.IsDigit(text[i]))
                {
                    return false;
                }
            }

            return true;
        }

        /// <summary>
        /// Prints a pretty game over screen.
        /// </summary>
        private static void PrintGameOver()
        {
            Console.SetCursorPosition(0, (Console.WindowHeight / 2) - 3);
            Console.WriteLine("  GGGG    AAA   MM    MM EEEEEEE   OOOOO  VV     VV EEEEEEE RRRRRR");
            Console.WriteLine(" GG  GG  AAAAA  MMM  MMM EE       OO   OO VV     VV EE      RR   RR");
            Console.WriteLine("GG      AA   AA MM MM MM EEEEE    OO   OO  VV   VV  EEEEE   RRRRRR");
            Console.WriteLine("GG   GG AAAAAAA MM    MM EE       OO   OO   VV VV   EE      RR  RR");
            Console.WriteLine(" GGGGGG AA   AA MM    MM EEEEEEE   OOOO0     VVV    EEEEEEE RR   RR");
            Console.WriteLine("               Press enter to continue");
        }
    }
}