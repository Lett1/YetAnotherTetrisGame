﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="TetrominoType.cs" company="LettSoft">
//   LettSoft 2018
// </copyright>
// <summary>
//   The tetromino type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace YetAnotherTetrisGame.Enums
{
    /// <summary>
    /// The different types of tetrominos.
    /// </summary>
    public enum TetrominoType
    {
        /// <summary>
        /// The long tetromino.
        /// </summary>
        I = 1,

        /// <summary>
        /// The block tetromino.
        /// </summary>
        O,

        /// <summary>
        /// The T tetromino.
        /// </summary>
        T,

        /// <summary>
        /// The J tetromino.
        /// </summary>
        J,

        /// <summary>
        /// The L tetromino.
        /// </summary>
        L,

        /// <summary>
        /// The S tetromino.
        /// </summary>
        S,

        /// <summary>
        /// The Z tetromino.
        /// </summary>
        Z
    }
}