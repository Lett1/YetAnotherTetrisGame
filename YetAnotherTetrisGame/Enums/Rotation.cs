﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Rotation.cs" company="LettSoft">
//   LettSoft 2018
// </copyright>
// <summary>
//   Defines rotation directions.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace YetAnotherTetrisGame.Enums
{
    /// <summary>
    /// Defines possible rotation directions.
    /// </summary>
    public enum Rotation
    {
        /// <summary>
        /// Rotates clockwise e.g. to the right.
        /// </summary>
        Clockwise,

        /// <summary>
        /// Rotates counterclockwise e.g. to the left.
        /// </summary>
        Counterclockwise
    }
}