﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="KeyboardInput.cs" company="LettSoft">
//   LettSoft 2018
// </copyright>
// <summary>
//   Defines the KeyboardInput type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace YetAnotherTetrisGame
{
    using System;

    using YetAnotherTetrisGame.Interfaces;

    /// <summary>
    /// Gets input from the keyboard.
    /// </summary>
    public class KeyboardInput : IInputManager
    {
        /// <inheritdoc />
        public bool InputAvailable()
        {
            return Console.KeyAvailable;
        }

        /// <inheritdoc />
        public ConsoleKey ReadKey()
        {
            return Console.ReadKey(true).Key;
        }

        /// <inheritdoc />
        public string ReadLine()
        {
            return Console.ReadLine();
        }

        /// <inheritdoc />
        public void WaitForKeyPress(params ConsoleKey[] keys)
        {
            while (true)
            {
                ConsoleKey key = Console.ReadKey(true).Key;
                if (Array.IndexOf(keys, key) > -1 || keys.Length == 0)
                {
                    break;
                }
            }
        }
    }
}