﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Tetromino.cs" company="LettSoft">
//   LettSoft 2018
// </copyright>
// <summary>
//   Defines a tetromino for use in Tetris.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace YetAnotherTetrisGame
{
    using System;

    using YetAnotherTetrisGame.Enums;

    /// <summary>
    /// Represents a single tetromino.
    /// </summary>
    public class Tetromino
    {
        /// <summary>
        /// The possible shapes of the tetromino.
        /// </summary>
        private int[][,] shapes;

        /// <summary>
        /// Initializes a new instance of the <see cref="T:YetAnotherTetrisGame.Tetromino"/> class.
        /// </summary>
        /// <param name="position">The position.</param>
        /// <param name="shapes">The shapes to use when rotating.</param>
        /// <param name="type">The tetromino type.</param>
        private Tetromino(Vector2 position, int[][,] shapes, TetrominoType type)
        {
            this.Position = position;
            this.shapes = shapes;
            this.Type = type;
            this.Rotation = 0;
            this.Landed = false;
        }

        /// <summary>
        /// Gets shape of the tetromino with the current rotation.
        /// </summary>
        /// <value>The current shape of the tetromino.</value>
        public int[,] Shape
        {
            get
            {
                return this.shapes[this.Rotation];
            }
        }

        /// <summary>
        /// Gets or sets the rotation of the block.
        /// </summary>
        /// <value>The rotation.</value>
        public int Rotation { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this <see cref="T:YetAnotherTetrisGame.Tetromino"/> has landed.
        /// </summary>
        /// <value><c>true</c> if it has landed; otherwise, <c>false</c>.</value>
        public bool Landed { get; set; }

        /// <summary>
        /// Gets or sets the position of the tetromino.
        /// </summary>
        /// <value>The position.</value>
        public Vector2 Position { get; set; }

        /// <summary>
        /// Gets the type of the tetromino.
        /// </summary>
        /// <value>The type of the tetromino.</value>
        public TetrominoType Type { get; private set; }

        /// <summary>
        /// Constructs a new tetromino object for the given shape.
        /// </summary>
        /// <param name="type">The type of Tetromino to create.</param>
        /// <param name="playField">The main playfield, used for collision.</param>
        /// <returns>A new Tetromino instance.</returns>
        public static Tetromino MakeTetromino(TetrominoType type, int[,] playField)
        {
            int playfieldCenter = (int)Math.Floor(playField.GetLength(0) / 2.0) - 2;

            return new Tetromino(new Vector2(playfieldCenter, -1), GetShapes(type), type);
        }

        /// <summary>
        /// Rotate the Tetromino in a direction.
        /// </summary>
        /// <param name="direction">Which direction to rotate the Tetromio in.</param>
        /// <exception cref="ArgumentOutOfRangeException">Thrown for invalid rotation types.</exception>
        public void Rotate(Rotation direction)
        {
            switch (direction)
            {
                case Enums.Rotation.Clockwise:
                    this.Rotation = ((this.Rotation - 1) + this.shapes.GetLength(0)) % this.shapes.GetLength(0);
                    break;
                case Enums.Rotation.Counterclockwise:
                    this.Rotation = (this.Rotation + 1) % this.shapes.GetLength(0);
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(direction), direction, null);
            }
        }

        /// <summary>
        /// Returns the shapes for the given TetrominoType.
        /// Shapes are arrays of 2D integer arrays (int[][,]), each of the 2d arrays defines the blocks for one rotation.
        /// Shapes are defined after http://tetris.wikia.com/wiki/Nintendo_Rotation_System.
        /// </summary>
        /// <param name="type">The type of Tetromino to generate the shapes for.</param>
        /// <returns>Jagged 2D Array for the shapes.</returns>
        /// <exception cref="ArgumentOutOfRangeException">Thrown for invalid Tetromino types.</exception>
        private static int[][,] GetShapes(TetrominoType type)
        {
            switch (type)
            {
                case TetrominoType.O:
                    return new int[][,]
                               {
                                   new int[,]
                                       {
                                            { 0, 1, 1, 0 }, { 0, 1, 1, 0 },
                                       } // □
                               };
                case TetrominoType.I:
                    return new int[][,]
                               {
                                   new int[,]
                                       {
                                           { 0, 0, 0, 0 }, { 0, 0, 0, 0 }, { 1, 1, 1, 1 },
                                           { 0, 0, 0, 0 }
                                       }, // -
                                   new int[,]
                                       {
                                           { 0, 0, 1, 0 }, { 0, 0, 1, 0 }, { 0, 0, 1, 0 },
                                           { 0, 0, 1, 0 }
                                       } // I
                               };
                case TetrominoType.J:
                    return new int[][,]
                               {
                                   new int[,] { { 0, 0, 0 }, { 1, 1, 1 }, { 0, 0, 1 } }, // ¬ 
                                   new int[,] { { 0, 1, 0 }, { 0, 1, 0 }, { 1, 1, 0 } }, // ᒧ 
                                   new int[,] { { 1, 0, 0 }, { 1, 1, 1 }, { 0, 0, 0 } }, // ⌙
                                   new int[,] { { 0, 1, 1 }, { 0, 1, 0 }, { 0, 1, 0 } }, // Γ
                               };
                case TetrominoType.L:
                    return new int[][,]
                               {
                                   new int[,] { { 0, 0, 0 }, { 1, 1, 1 }, { 1, 0, 0 } }, // ⌐
                                   new int[,] { { 1, 1, 0 }, { 0, 1, 0 }, { 0, 1, 0 } }, // ⅂
                                   new int[,] { { 0, 0, 1 }, { 1, 1, 1 }, { 0, 0, 0 } }, // ⏗
                                   new int[,] { { 0, 1, 0 }, { 0, 1, 0 }, { 0, 1, 1 } } // L
                               };
                case TetrominoType.T:
                    return new int[][,]
                               {
                                   new int[,] { { 0, 0, 0 }, { 1, 1, 1 }, { 0, 1, 0 } }, // T
                                   new int[,] { { 0, 1, 0 }, { 1, 1, 0 }, { 0, 1, 0 } }, // ⊣
                                   new int[,] { { 0, 1, 0 }, { 1, 1, 1 }, { 0, 0, 0 } }, // ⊥
                                   new int[,] { { 0, 1, 0 }, { 0, 1, 1 }, { 0, 1, 0 } }, // ⊢
                               };
                case TetrominoType.S:
                    return new int[][,]
                               {
                                   new int[,] { { 0, 0, 0 }, { 0, 1, 1 }, { 1, 1, 0 } }, // S
                                   new int[,] { { 0, 1, 0 }, { 0, 1, 1 }, { 0, 0, 1 } } // ഗ
                               };
                case TetrominoType.Z:
                    return new int[][,]
                               {
                                   new int[,] { { 0, 0, 0 }, { 1, 1, 0 }, { 0, 1, 1 } }, // Z
                                   new int[,] { { 0, 0, 1 }, { 0, 1, 1 }, { 0, 1, 0 } }, // Z on the side
                               };
                default:
                    throw new ArgumentOutOfRangeException(nameof(type), type, null);
            }
        }
    }
}