﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="TetrisGame.cs" company="LettSoft">
//   LettSoft (c) 2018
// </copyright>
// <summary>
//   Implements a full game of Tetris.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace YetAnotherTetrisGame
{
    using System;
    using System.Threading;

    using YetAnotherTetrisGame.Enums;

    /// <summary>
    /// Implements a full game of Tetris.
    /// </summary>
    public class TetrisGame
    {
        /// <summary>
        /// The height of the playfield.
        /// </summary>
        private const int Playfieldheight = 20;

        /// <summary>
        /// The width of the playfield.
        /// </summary>
        private const int Playfieldwidth = 10;

        /// <summary>
        /// The currently falling block.
        /// </summary>
        private Tetromino block;

        /// <summary>
        /// The current level fo the game.
        /// </summary>
        private int level;

        /// <summary>
        /// The next block thats going to fall.
        /// </summary>
        private Tetromino nextBlock;

        /// <summary>
        /// The field on which the tetrominos fall.
        /// </summary>
        private int[,] playField;

        /// <summary>
        /// The players total points.
        /// </summary>
        private int points;

        /// <summary>
        /// IntervalTimer that moves the block downwards.
        /// </summary>
        private IntervalTimer timer;

        /// <summary>
        /// The total lines cleared by the player.
        /// </summary>
        private int totalLinesCleared;

        /// <summary>
        /// Initializes a new instance of the <see cref="T:YetAnotherTetrisGame.TetrisGame"/> class.
        /// </summary>
        /// <param name="level">The game level to start on. Defaults to 1.</param>
        public TetrisGame(int level = 1)
        {
            this.points = 0;
            this.totalLinesCleared = 0;
            this.level = level;

            this.timer = new IntervalTimer(0);
            this.timer.OnIntervalPassed += this.Update;

            this.playField = new int[Playfieldwidth, Playfieldheight];
        }

        /// <summary>
        /// Start the game.
        /// </summary>
        /// <returns>True if the user quit manually, false if a game over was reached.</returns>
        public bool Run()
        {
            TetrominoStream stream = new TetrominoStream();

            // Spawn the initial Tetromino
            this.nextBlock = Tetromino.MakeTetromino(TetrominoType.T, this.playField);
            this.SpawnTetromino(stream.Pop());

            while (true)
            {
                this.Draw();

                while (Services.Input.InputAvailable())
                {
                    ConsoleKey key = Console.ReadKey(true).Key;

                    switch (key)
                    {
                        case ConsoleKey.Escape:
                            this.timer.Stop();
                            return true;
                        case ConsoleKey.UpArrow:
                            this.RotateTetromino(Rotation.Counterclockwise);
                            break;
                        case ConsoleKey.DownArrow:
                            this.RotateTetromino(Rotation.Clockwise);
                            break;
                        case ConsoleKey.LeftArrow:
                            if (this.CanMoveTo(new Vector2(-1, 0)))
                            {
                                this.block.Position = new Vector2(this.block.Position.X - 1, this.block.Position.Y);
                            }

                            break;
                        case ConsoleKey.RightArrow:
                            if (this.CanMoveTo(new Vector2(1, 0)))
                            {
                                this.block.Position = new Vector2(this.block.Position.X + 1, this.block.Position.Y);
                            }

                            break;
                        case ConsoleKey.Spacebar:
                            this.timer.ChangeInterval(0);
                            break;
                    }
                }

                if (this.block.Landed)
                {
                    // Stop the block from falling any further
                    this.timer.Stop();
                    
                    // Embed the block into the playfield
                    for (int i = 0; i < this.block.Shape.GetLength(0); i++)
                    {
                        for (int j = 0; j < this.block.Shape.GetLength(1); j++)
                        {
                            if (this.block.Shape[i, j] != 0)
                            {
                                this.playField[i + this.block.Position.X, j + this.block.Position.Y] = (int)this.block.Type;
                            }
                        }
                    }

                    this.CheckForLineClears();

                    // Spawn a new tetronomino
                    if (!this.SpawnTetromino(stream.Pop()))
                    {
                        return false;
                    }
                }

                Thread.Sleep(50);
            }
        }

        /// <summary>
        /// Tests of the tetromino can move in the given direction.
        /// </summary>
        /// <param name="direction">The direction to move in.</param>
        /// <returns>True of movement is possible, false otherwise.</returns>
        public bool CanMoveTo(Vector2 direction)
        {
            Vector2 newPosition = this.block.Position.Add(direction);

            for (int i = 0; i < this.block.Shape.GetLength(0); i++)
            {
                for (int j = 0; j < this.block.Shape.GetLength(1); j++)
                {
                    if (this.block.Shape[i, j] != 0)
                    {
                        // The block will hit the bottom of the playField.
                        if (j + newPosition.Y >= this.playField.GetLength(1))
                        {
                            return false;
                        }

                        // Prevent the block from going through the side walls.
                        if (i + newPosition.X >= this.playField.GetLength(0) || i + newPosition.X < 0)
                        {
                            return false;
                        }

                        // Handle special case when the tetromino still hasn't moved down.
                        if (newPosition.Y == -1)
                        {
                            return true;
                        }

                        // Check if we hit another landed block.
                        if (this.playField[i + this.block.Position.X + direction.X, j + this.block.Position.Y + direction.Y] != 0)
                        {
                            return false;
                        }
                    }
                }
            }

            return true;
        }

        /// <summary>
        /// Removes the selected line and shifts the lines above it downwards.
        /// </summary>
        /// <param name="array">
        /// The array to process.
        /// </param>
        /// <param name="startRow">
        /// At which row to start.
        /// </param>
        private static void ShiftArrayDown(int[,] array, int startRow)
        {
            if (startRow > array.GetLength(1))
            {
                throw new ArgumentOutOfRangeException(
                    nameof(startRow),
                    "Starting row was outside the bounds of the array.");
            }

            if (startRow < 0)
            {
                throw new ArgumentOutOfRangeException(nameof(startRow), "Starting row cannot be less than 0.");
            }

            for (int row = startRow; row > 0; row--)
            {
                for (int j = 0; j < array.GetLength(0); j++)
                {
                    array[j, row] = array[j, row - 1];
                }
            }
        }

        /// <summary>
        /// Calculates the speed of the tetromino according to the current level.
        /// </summary>
        /// <returns>
        /// The speed of the block.
        /// </returns>
        /// <param name="level">
        /// The current level.
        /// </param>
        private static int CalculateSpeed(int level)
        {
            if (level <= 5)
            {
                return 500;
            }
            else if (level > 5 && level <= 10)
            {
                return 400;
            }
            else if (level > 10 && level <= 15)
            {
                return 300;
            }
            else if (level > 15 && level <= 20)
            {
                return 200;
            }
            else if (level > 20)
            {
                return 100;
            }

            // Catch all
            return 500;
        }

        /// <summary>
        /// Draw the playfield, the falling tetromino and the hud.
        /// </summary>
        private void Draw()
        {
            Services.Graphics.Clear();

            // Draw the side walls
            for (int i = 0; i <= this.playField.GetLength(1); i++)
            {
                Services.Graphics.PutString(0, i, "#");
                Services.Graphics.PutString(this.playField.GetLength(0) + 1, i, "#");
            }

            // Draw the bottom wall
            for (int i = 0; i <= this.playField.GetLength(0) + 1; i++)
            {
                Services.Graphics.PutString(i, this.playField.GetLength(1) + 1, "#");
            }

            // Draw HUD
            Services.Graphics.PutString(this.playField.GetLength(0) + 3, 1, $"Score: {this.points}");
            Services.Graphics.PutString(this.playField.GetLength(0) + 3, 2, $"Level: {this.level}");
            Services.Graphics.PutString(this.playField.GetLength(0) + 3, 4, "Next:");

            // Draw the play field
            for (int i = 0; i < this.playField.GetLength(0); i++)
            {
                for (int j = 0; j < this.playField.GetLength(1); j++)
                {
                    if (this.playField[i, j] != 0)
                    {
                        Services.Graphics.SetBackgroundColor(
                            this.GetColorForTetromino((TetrominoType)this.playField[i, j]));
                        Services.Graphics.PutString(i + 1, j + 1, " ");
                    }
                }
            }

            // Draw the currently falling tetromino
            for (int i = 0; i < this.block.Shape.GetLength(0); i++)
            {
                for (int j = 0; j < this.block.Shape.GetLength(1); j++)
                {
                    if (this.block.Position.X + i >= 0 && this.block.Shape[i, j] != 0)
                    {
                        Services.Graphics.SetBackgroundColor(this.GetColorForTetromino(this.block.Type));
                        Services.Graphics.PutString(this.block.Position.X + i + 1, this.block.Position.Y + j + 1, " ");
                    }
                }
            }
            
            // Draw next tetromino into the HUD
            for (int i = 0; i < this.nextBlock.Shape.GetLength(0); i++)
            {
                for (int j = 0; j < this.nextBlock.Shape.GetLength(1); j++)
                {
                    if (this.nextBlock.Position.X + i >= 0 && this.nextBlock.Shape[i, j] != 0)
                    {
                        Services.Graphics.SetBackgroundColor(this.GetColorForTetromino(this.nextBlock.Type));
                        Services.Graphics.PutString(this.playField.GetLength(0) + 3 + i, j + 5, " ");
                    }
                }
            }

            Services.Graphics.ResetColors();
        }

        /// <summary>
        /// Creates a new tetromino and checks if it can move downwards.
        /// </summary>
        /// <param name="type">What kind of tetromino to create.</param>
        /// <returns>True of the block is free to fall, false if its stuck.</returns>
        private bool SpawnTetromino(TetrominoType type)
        {
            this.block = this.nextBlock;
            this.nextBlock = Tetromino.MakeTetromino(type, this.playField);

            if (!this.CanMoveTo(new Vector2(0, 1)))
            {
                // Draw the game once more to show the final block
                this.Draw();
                return false;
            }
            
            this.timer.ChangeInterval(CalculateSpeed(this.level));
            this.timer.Start();

            return true;
        }

        /// <summary>
        /// Rotates the tetromino if possible.
        /// </summary>
        /// <param name="rotationDirection">In which direction to rotate.</param>
        private void RotateTetromino(Rotation rotationDirection)
        {
            int oldRotation = this.block.Rotation;

            this.block.Rotate(rotationDirection);

            // If the rotation would've put the tetromino through a block or wall, reverse it.
            if (!this.CanMoveTo(new Vector2(0, 0)))
            {
                this.block.Rotation = oldRotation;
            }
        }

        /// <summary>
        /// Move the Tetromino down if possible, otherwise set it as landed and stop the timer.
        /// </summary>
        /// <param name="sender">Event sender.</param>
        /// <param name="e">Event arguments.</param>
        private void Update(object sender, EventArgs e)
        {
            // Check if the tetromino can fall down
            if (this.CanMoveTo(new Vector2(0, 1)))
            {
                // If the interval is 0, the block is falling instantly, give the player a point for each row.
                if (this.timer.Interval == 0)
                {
                    this.points++;
                }

                this.block.Position = new Vector2(this.block.Position.X, this.block.Position.Y + 1);
            }
            else
            {
                this.block.Landed = true;
            }
        }

        /// <summary>
        /// Checks if a line is full and removes lines if necessary.
        /// </summary>
        private void CheckForLineClears()
        {
            int clearedLines = 0;

            for (int row = 0; row < this.playField.GetLength(1); row++)
            {
                bool lineFull = true;

                for (int collumn = 0; collumn < this.playField.GetLength(0); collumn++)
                {
                    if (this.playField[collumn, row] == 0)
                    {
                        lineFull = false;
                        break;
                    }
                }

                if (lineFull)
                {
                    this.totalLinesCleared++;

                    // Increase the level every 10 lines cleared
                    if (this.totalLinesCleared > 9)
                    {
                        this.level++;
                        this.totalLinesCleared = 0;
                    }

                    clearedLines++;
                    ShiftArrayDown(this.playField, row);
                }
            }

            // Award the player with points, more if multiple lines were cleared at once
            switch (clearedLines)
            {
                case 1:
                    this.points += this.level * 40;
                    break;
                case 2:
                    this.points += this.level * 100;
                    break;
                case 3:
                    this.points += this.level * 300;
                    break;
                case 4:
                    this.points += this.level * 1200;
                    break;
            }
        }

        /// <summary>
        /// Gets the color for the given tetromino.
        /// </summary>
        /// <returns>
        /// The color for the tetromino.
        /// </returns>
        /// <param name="type">
        /// The type of tetromino.
        /// </param>
        private ConsoleColor GetColorForTetromino(TetrominoType type)
        {
            switch (type)
            {
                case TetrominoType.I:
                    return ConsoleColor.Cyan;
                case TetrominoType.O:
                    return ConsoleColor.Yellow;
                case TetrominoType.T:
                    return ConsoleColor.Green;
                case TetrominoType.J:
                    return ConsoleColor.Red;
                case TetrominoType.L:
                    return ConsoleColor.Magenta;
                case TetrominoType.S:
                    return ConsoleColor.White;
                case TetrominoType.Z:
                    return ConsoleColor.Blue;
                default:
                    throw new ArgumentOutOfRangeException(nameof(type), type, null);
            }
        }
    }
}